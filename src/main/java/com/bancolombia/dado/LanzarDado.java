package com.bancolombia.dado;

public class LanzarDado {
	
	private GenerarNumero generarNumero; 
	
	public LanzarDado(GenerarNumero generarNumero) {
		this.generarNumero = generarNumero;
	}

	public String lanzar() {
		int numero = generarNumero.getNumero();
		if (numero==5) {
			return "Ganaste";
		} else {
			return "Perdiste";
		}
		
	}

}
