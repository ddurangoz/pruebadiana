package com.bancolombia.dado;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;


public class LanzarDadoTest {
	
	@Test
	public void debeMostrarGanasteCuandoSea5(){
		//Arrange
		GenerarNumero generarNumero = Mockito.mock(GenerarNumero.class);
		Mockito.when(generarNumero.getNumero()).thenReturn(5);
		LanzarDado lanzardado = new LanzarDado(generarNumero);
		//Act
		String resultado = lanzardado.lanzar();
		//Assert
		Assert.assertEquals("Ganaste", resultado);
	}
	
	@Test
	public void debeMostrarGanasteCuandoSea3(){
		//Arrange
		GenerarNumero generarNumero = Mockito.mock(GenerarNumero.class);
		Mockito.when(generarNumero.getNumero()).thenReturn(3);
		LanzarDado lanzardado = new LanzarDado(generarNumero);
		
		//Act
		String resultado = lanzardado.lanzar();
		//Assert
		Assert.assertEquals("Perdiste", resultado);
	}

}
